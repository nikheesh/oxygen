package info.androidhive.imageslider;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.greenwichnexus.oxygen.R;









import info.androidhive.imageslider.adapter.FullScreenImageAdapter;
import info.androidhive.imageslider.helper.AppConstant;
import info.androidhive.imageslider.helper.Utils;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

public class FullScreenViewActivity extends Activity {

	private Utils utils;
	private FullScreenImageAdapter adapter;
	private ViewPager viewPager;
	
	ProgressDialog pDialog;

	public ArrayList<String> fileAddress = new ArrayList<String>();
	int count = 0;
	String serverURL;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_fullscreen_view);

		
		
		
		
		
		
		viewPager = (ViewPager) findViewById(R.id.pager);

		Intent i = getIntent();
		int position = i.getIntExtra("position", 0);

		utils = new Utils(getApplicationContext());

		
		
		
		serverURL = AppConstant.SERVER_URL;

		// Create Object and call AsyncTask execute Method
		new LongOperation().execute(serverURL);

		adapter = new FullScreenImageAdapter(FullScreenViewActivity.this,
				utils.getFileAddress());

		viewPager.setAdapter(adapter);

		// displaying selected image first
		viewPager.setCurrentItem(position);
	}

	// Class with extends AsyncTask class
	private class LongOperation extends AsyncTask<String, Void, Void> {

		private final HttpClient Client = new DefaultHttpClient();
		private String Content;
		private String Error = null;

		protected void onPreExecute() {
			// NOTE: You can call UI Element here.
			
            // Showing progress dialog
            pDialog = new ProgressDialog(FullScreenViewActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(true);
            pDialog.setOnCancelListener(new OnCancelListener() {
				
				@Override
				public void onCancel(DialogInterface dialog) {
					// TODO Auto-generated method stub
					cancel(true);
					finish();
					
				}
			});
            pDialog.show();			super.onPreExecute();
		}

		// Call after onPreExecute method
		protected Void doInBackground(String... urls) {
			try {

				// Call long running operations here (perform background
				// computation)
				// NOTE: Don't call UI Element here.

				// Server url call by GET method
				HttpGet httpget = new HttpGet(urls[0]);
				ResponseHandler<String> responseHandler = new BasicResponseHandler();
				Content = Client.execute(httpget, responseHandler);

			} catch (ClientProtocolException e) {
				Error = e.getMessage();
				cancel(true);
			} catch (IOException e) {
				Error = e.getMessage();
				cancel(true);
			}

			return null;
		}

		protected void onPostExecute(Void unused) {
			// NOTE: You can call UI Element here.

			fileAddress = new ArrayList<String>();
			if (Error != null) {

				Log.d("Message", "Output : " + Error);

			} else {

				Log.d("Message", "Output : " + Content);
				try {
					JSONObject jsonObj = new JSONObject(Content);
					count = jsonObj.getInt("count");
					Log.e("count", "" + count);
					JSONArray jArray = jsonObj.getJSONArray("image");
					for (int i = 0; i < jArray.length(); i++) {
						JSONObject innrObj = jArray.getJSONObject(i);
						fileAddress.add(innrObj.getString("image_name"));
						Log.d("Address", innrObj.getString("image_name"));
						Log.d("File Address", fileAddress.get(i));

					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//Toast.makeText(getApplicationContext(), "Done On Progress", Toast.LENGTH_SHORT).show();
				utils = new Utils(getApplicationContext(), fileAddress, count);
				adapter = new FullScreenImageAdapter(
						FullScreenViewActivity.this, utils.getFileAddress());
				viewPager.setAdapter(adapter);
			}
			
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
		}

	}
}
