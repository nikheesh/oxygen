package info.androidhive.imageslider.helper;

import java.util.Arrays;
import java.util.List;

public class AppConstant {

	// Server URL
	//public static final String SERVER_URL = "http://androidbeyondapps.com/imageGallery/action.php?action=sendimages";
	public static final String SERVER_URL = "http://www.cochinfood.com/sahodaya/admin/action.php?action=sendimages";
	// Number of columns of Grid View
	public static final int NUM_OF_COLUMNS = 3;

	// Gridview image padding
	public static final int GRID_PADDING = 8; // in dp

	// SD card image directory
	public static final String PHOTO_ALBUM = "DCIM/Facebook";

	// supported file formats
	public static final List<String> FILE_EXTN = Arrays.asList("jpg", "jpeg",
			"png");
}
