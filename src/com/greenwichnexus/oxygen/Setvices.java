package com.greenwichnexus.oxygen;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;

public class Setvices  extends Activity{
	
	
	ImageView ph1,ph2;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.services);
		
		ph1=(ImageView) findViewById(R.id.ph_no);
		ph1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent callIntent = new Intent(Intent.ACTION_VIEW);
			    callIntent.setData(Uri.parse("tel:+919020100100"));
			    startActivity(callIntent);	
				
			}
		});
		
		ph2=(ImageView) findViewById(R.id.phone);
		ph2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent callIntent = new Intent(Intent.ACTION_VIEW);
			    callIntent.setData(Uri.parse("tel:+919020200200"));
			    startActivity(callIntent);	
				
			}
		});
		
	}

}
