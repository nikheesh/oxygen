package com.greenwichnexus.oxygen;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

public class Notification extends Activity{
	TextView tvTitle;
	TextView tvMessage;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.notification);
		tvTitle=(TextView)findViewById(R.id.tvTitle);
		tvMessage=(TextView)findViewById(R.id.tvMessage);
		parse(getIntent().getStringExtra("json"));
		//cancelNotification(getApplicationContext(), 777);
	}

	private void parse(String json)
	{
		String title="",message="No Message";
		try {
			JSONObject messageJson = new JSONObject(json);
			title=messageJson.getString("header");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		tvTitle.setText(title);
		try {
			JSONObject messageJson = new JSONObject(json);
			message=messageJson.getString("title");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		tvMessage.setText(message);
		Toast.makeText(getApplicationContext(), message, 0).show();
		//Log.i("tvTitle", "Activity created");
	}
	
	public static void cancelNotification(Context ctx, int notifyId) {
	    //String ns = MainActivity.NOTIFICATION_TAG;
	    NotificationManager nMgr = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
	    nMgr.cancel(notifyId);
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		Data.APP_RUNNING=false;
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Data.APP_RUNNING=true;
	}

}
