package com.greenwichnexus.oxygen;

import java.io.File;
import java.util.ArrayList;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.koushikdutta.async.future.Future;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

public class CompanyNamesListActivity extends Activity {
    // adapter that holds tweets, obviously :)
    ArrayAdapter<ListCompany> tweetAdapter;
    ArrayList<ListCompany> listcompany;
    ListCompany listcomp;
    String url;
    WebView loadgif;
  //  ProgressBar progress;
   
    int count;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	@SuppressWarnings("deprecation")
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        count=-1;
     

        
        tweetAdapter = new ArrayAdapter<ListCompany>(this, 0) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if (convertView == null)
                    convertView = getLayoutInflater().inflate(R.layout.company_name_item, null);

             
                Button handle = (Button)convertView.findViewById(R.id.companyname);
              handle.setText(this.getItem(position).name);
              final ListCompany listcompany=this.getItem(position);
              handle.setOnClickListener(new OnClickListener() {
      			
      			@Override
      			public void onClick(View v) {
      				Intent i=new Intent(getApplicationContext(),ProductListActivity.class);
      				i.putExtra("catid",listcompany.catid);
      				i.putExtra("compid", listcompany.id);
      				i.putExtra("isoffer", listcompany.isoffer);
      				startActivity(i);
      				// TODO Auto-generated method stub
      				
      			}
      		});

                return convertView;
            }
        };
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        // basic setup of the ListView and adapter
        setContentView(R.layout.companynameslistactivity);
        
        int sdk = android.os.Build.VERSION.SDK_INT;
    //    loadgif=(WebView)findViewById(R.id.loadinggif);
        ListView listView = (ListView)findViewById(R.id.list_company);
        ImageView background=(ImageView)findViewById(R.id.heading);
        String catname= getIntent().getExtras().getString("id").toString();
        String catid="0";
        String catofferid="0";
        if(catname.equals("mob")){
        	catid="31";
        	
        	    background.setImageResource(R.drawable.head_mobile);
//        	} else {
//        	    background.setBackground( getResources().getDrawable(R.drawable.head_mobile));
//        	}
        }
        if(catname.equals("tab")){
        	catid="33";
        	 background.setImageResource(R.drawable.head_tablet);
//        	if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
//        	    background.setBackgroundDrawable( getResources().getDrawable(R.drawable.head_tablet) );
//        	} else {
//        	    background.setBackground( getResources().getDrawable(R.drawable.head_tablet));
//        	}
        	}
        if(catname.equals("access")){
        	catid="34";
        	 background.setImageResource(R.drawable.head_acces);
//        	if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
//        	    background.setBackgroundDrawable( getResources().getDrawable(R.drawable.head_acces) );
//        	} else {
//        	    background.setBackground( getResources().getDrawable(R.drawable.head_acces));
//        	}
        }
        if(catname.equals("desk")){
        	catid="35";
        	 background.setImageResource(R.drawable.head_desktop);
//        	if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
//        	    background.setBackgroundDrawable( getResources().getDrawable(R.drawable.head_desktop) );
//        	} else {
//        	    background.setBackground( getResources().getDrawable(R.drawable.head_desktop));
//        	}
        }
      
        	 if(catname.equals("lap")){
             	catid="32";
             	 background.setImageResource(R.drawable.head_laptop);
//             	if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
//            	    background.setBackgroundDrawable( getResources().getDrawable(R.drawable.head_laptop) );
//            	} else {
//            	    background.setBackground( getResources().getDrawable(R.drawable.head_laptop));
//            	}
        	 }
        	 if(catname.equals("offers")){
        		// background.setBackgroundDrawable(getResources().getDrawable(R.drawable.offers1));
              	catid="offers";
              	 background.setImageResource(R.drawable.head_offer);
              	 catofferid= getIntent().getExtras().getString("catid").toString();
//              	if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
//            	    background.setBackgroundDrawable( getResources().getDrawable(R.drawable.head_offer) );
//            	} else {
//            	    background.setBackground( getResources().getDrawable(R.drawable.head_offer));
//            	}
        	 }
	     
        	loadfromfile(catid,catofferid);
        
        listView.setAdapter(tweetAdapter);

    }

      Future<JsonArray> loading,loadingfile;
    


    private void loadfromfile(final String catid,final String catofferid)
    {
    	loadingfile= Ion.with(this)
    	        .load(new File(new File(getCacheDir(),"")+"cacheFileAll.srl"))
    	        
    	        
    	        .asJsonArray()
    	        
    	        .setCallback(new FutureCallback<JsonArray>() {
    	            @Override
    	            public void onCompleted(Exception e, JsonArray result) {
    	                // this is called back onto the ui thread, no Activity.runOnUiThread or Handler.post necessary.
    	                if (e != null) {
    	                    Toast.makeText(getApplicationContext(), "Your connection is too slow, please wait few seconds while we are fetching latest data", Toast.LENGTH_LONG).show();
    	                    return;
    	                   
    	                }
    	              
    	               listcompany=new ArrayList<ListCompany>();
    	               if(catid.equals("offers")){
    	            	   
    	            	   for (int i = 0; i < result.size(); i++) {
    	  	                	if(result.get(i).getAsJsonObject().get("isOffer").getAsString().equals("1") && result.get(i).getAsJsonObject().get("categoryId").getAsString().equals(catofferid))
    	  	                	{
    	  	                		Log.e(" =======catid========",result.get(i).getAsJsonObject().get("categoryId").getAsString());
    	  	                		String id=result.get(i).getAsJsonObject().get("manufacturerId").getAsString();
    	  	                		Log.e("==items id==", id);
    	  	                		String name=result.get(i).getAsJsonObject().get("manufacturerName").getAsString();
    	  	                		Log.e("==items name==", name);
    	  	                		if(listcompany.size()==0)
    	  	                			{
    	  	                	listcomp =new ListCompany();
    	  	              
    	  	                	listcomp.id=id;
    	  	                	listcomp.catid=catofferid;
    	  	                	listcomp.isoffer="1";
    	  	                //	Log.e("====man  id", listcomp.id);
    	  	                	listcomp.name=name;
    	  	                	listcompany.add(listcomp);
    	  	                	//Log.e("====man  id", listcomp.name);
    	  	              }
    	  	              else {
    	  	            	  
    	  	            	  int f=0;
    	  	            	  for(int j=0;j<listcompany.size();j++)
    	  	            	  		{ 		if(listcompany.get(j).id.equals(id))
    	  	            	  					{f=1;
    	  	            	  					break;
    	  	            	  					}
    	  	            	  		}
    	  	            	  
    	  	            	  if(f==0)
    	  	            	  	{
    	  	            		  listcomp =new ListCompany();
    	  	            		  listcomp.id=id;
    	  	            		  listcomp.catid=catofferid;
    	  	            		  listcomp.isoffer="1";
    		                	//Log.e("====man  id", listcomp.id);
    		                	listcomp.name=name;
    	  	            		listcompany.add(listcomp);  
    	  	            	  	}
    	  	            	  
    	  	              	}
    	  	                	
    	  	                	}
    	    	                   // tweetAdapter.add(result.get(i).getAsJsonObject());
    	    	                }
    	            	   
    	            	   
    	               }
    	               else{
    	           	
  	                for (int i = 0; i < result.size(); i++) {
  	                	if(result.get(i).getAsJsonObject().get("categoryId").getAsString().equals(catid))
  	                	{
  	                		Log.e(" =======catid========",result.get(i).getAsJsonObject().get("categoryId").getAsString());
  	                		String id=result.get(i).getAsJsonObject().get("manufacturerId").getAsString();
  	                		Log.e("==items id==", id);
  	                		String name=result.get(i).getAsJsonObject().get("manufacturerName").getAsString();
  	                		Log.e("==items name==", name);
  	                		if(listcompany.size()==0)
  	                			{
  	                	listcomp =new ListCompany();
  	              
  	                	listcomp.id=id;
  	                	listcomp.catid=catid;
  	                	listcomp.isoffer="0";
  	                //	Log.e("====man  id", listcomp.id);
  	                	listcomp.name=name;
  	                	listcompany.add(listcomp);
  	                	//Log.e("====man  id", listcomp.name);
  	              }
  	              else {
  	            	  
  	            	  int f=0;
  	            	  for(int j=0;j<listcompany.size();j++)
  	            	  		{ 		if(listcompany.get(j).id.equals(id))
  	            	  					{f=1;
  	            	  					break;
  	            	  					}
  	            	  		}
  	            	  
  	            	  if(f==0)
  	            	  	{
  	            		  listcomp =new ListCompany();
  	            	  listcomp.id=id;
  	            	listcomp.catid=catid;
  	            	listcomp.isoffer="0";
	                	//Log.e("====man  id", listcomp.id);
	                	listcomp.name=name;
  	            		listcompany.add(listcomp);  
  	            	  	}
  	            	  
  	              	}
  	                	
  	                	}
    	                   // tweetAdapter.add(result.get(i).getAsJsonObject());
    	                }
    	               }
//  	            
  	            for (int i = 0; i < listcompany.size(); i++) {
  	            	tweetAdapter.add(listcompany.get(i));
  	            	Log.e("ites",listcompany.get(i).toString());
  	            	
  	            	
  	            }  
    					
    	            }
    	        });
    }
   

}
