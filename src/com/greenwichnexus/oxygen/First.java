package com.greenwichnexus.oxygen;

import java.io.File;

import com.arellomobile.android.push.BasePushMessageReceiver;
import com.arellomobile.android.push.PushManager;
import com.arellomobile.android.push.utils.RegisterBroadcastReceiver;
import com.koushikdutta.async.future.Future;
import com.koushikdutta.ion.Ion;




import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.Toast;

public class First extends Activity{
	
	
	//push//
	ApplicationClass appState;
			private static final String APP_ID = "11E45-CF594";//pushwush appid
			private static final String SENDER_ID = "465207938572";//google project ID

			boolean broadcastPush = true;
		//push dogwood-thought-715// 240615850876//	
			 private ConnectivityManager conmgr;
				 File file;
		//	FileStoring loadfiles;
			
	ImageView mob,tab,lap,des,acce,offr,abt,serv,bran,social;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		 requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.first);
//		
		
		  appState = ((ApplicationClass)getApplicationContext());
         
		
	file = new File(new File(getCacheDir(),"")+"cacheFileAll.srl");
		
		conmgr = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
//		if(!file.exists())
//		if(loadfiles.checkInternet())
//		new Filestoring().execute();
		 //push//
        registerReceivers();

		//Create and start push manager
		PushManager pushManager = new PushManager(this, APP_ID, SENDER_ID);
		pushManager.onStartup(this);
		PushManager.setMultiNotificationMode(this);
		checkMessage(getIntent(),true);
		
		//push//
		
		 mob=(ImageView) findViewById(R.id.mobile);
		mob.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				startnextactivity("mob");
				
				// TODO Auto-generated method stub
				
			}
		});
		tab=(ImageView) findViewById(R.id.tablets);
		tab.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
//				Intent i=new Intent(getApplicationContext(),CompanyNamesListActivity.class);
//				i.putExtra("id","tab");
//				startActivity(i);
				startnextactivity("tab");
				// TODO Auto-generated method stub
				
			}
		});
		lap=(ImageView) findViewById(R.id.laptop);
		lap.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
//				Intent i=new Intent(getApplicationContext(),CompanyNamesListActivity.class);
//				i.putExtra("id","lap");
//				startActivity(i);
				startnextactivity("lap");
				// TODO Auto-generated method stub
				
			}
		});
		des=(ImageView) findViewById(R.id.desktop);
		des.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
//				Intent i=new Intent(getApplicationContext(),CompanyNamesListActivity.class);
//				i.putExtra("id","desk");
//				startActivity(i);
				startnextactivity("desk");
				// TODO Auto-generated method stub
				
			}
		});
		acce=(ImageView) findViewById(R.id.accessories);
		acce.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
//				Intent i=new Intent(getApplicationContext(),CompanyNamesListActivity.class);
//				i.putExtra("id","access");
//				startActivity(i);
				startnextactivity("access");
				// TODO Auto-generated method stub
				
			}
		});
		
		offr=(ImageView) findViewById(R.id.offers);
		offr.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
//				Intent i=new Intent(getApplicationContext(),OfferActivity.class);
//				i.putExtra("id","offers");
//				startActivity(i);
				//startnextactivity("offers");
				
				
				if(appState.asynctask.getStatus()==AsyncTask.Status.FINISHED)
				{
					if(file.exists()){
						Intent i=new Intent(getApplicationContext(),OfferActivity.class);
						i.putExtra("id","offers");
						startActivity(i);
					}
//					else{
//						Toast.makeText(getApplicationContext(), "Your internet is too slow..please wait", Toast.LENGTH_LONG).show();
//					}
				}
				
				else if(appState.asynctask.getStatus()==AsyncTask.Status.PENDING) {
					if(file.exists()){
						Intent i=new Intent(getApplicationContext(),OfferActivity.class);
						i.putExtra("id","offers");
						startActivity(i);
					}
					
				}
				else if(appState.asynctask.getStatus()==AsyncTask.Status.RUNNING){
//					else{
					Toast.makeText(getApplicationContext(), "Your connection is too slow, please wait few seconds while we are fetching latest data", Toast.LENGTH_LONG).show();
//				}
		//appState.startLoading();
		//new checkstatusofloading().execute(id);
				}
				
				
				
				// TODO Auto-generated method stub
				
			}
		});
		abt=(ImageView) findViewById(R.id.about_us);
		abt.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent i=new Intent(getApplicationContext(),Web.class);
				i.putExtra("id","file:///android_asset/about_us.html");
				startActivity(i);
				
				
			}
		});
		serv=(ImageView) findViewById(R.id.services);
		serv.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),Setvices.class);
				startActivity(i);
				
				
				
			}
		});
		bran=(ImageView) findViewById(R.id.branches);
		bran.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent i=new Intent(getApplicationContext(),Web.class);
				i.putExtra("id","file:///android_asset/branch.html");
				startActivity(i);
				
			}
		});
		social=(ImageView) findViewById(R.id.social);
		social.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),Socialcntct.class);
				startActivity(i);
				
			}
		});
	}
		
	private void startnextactivity(String id)
	{
		Log.d("NEXTACT", "asyntask status is"+appState.asynctask.getStatus().toString());
		
		if(appState.asynctask.getStatus()==AsyncTask.Status.FINISHED)
		{
			if(file.exists() && file.length() > 1024){
				Intent i=new Intent(getApplicationContext(),CompanyNamesListActivity.class);
				i.putExtra("id",id);
				startActivity(i);
			}
			else{
				Toast.makeText(getApplicationContext(), "Your connection is too slow, please wait few seconds while we are fetching latest data", Toast.LENGTH_LONG).show();
			}
		}
		
		else if(appState.asynctask.getStatus()==AsyncTask.Status.PENDING) {
			if(file.exists()){
				Intent i=new Intent(getApplicationContext(),CompanyNamesListActivity.class);
				i.putExtra("id",id);
				startActivity(i);
			}
			
		}
		else if(appState.asynctask.getStatus()==AsyncTask.Status.RUNNING){
//			else{
			Toast.makeText(getApplicationContext(), "Your connection is too slow, please wait few seconds while we are fetching latest data", Toast.LENGTH_LONG).show();
//		}
//appState.startLoading();
//new checkstatusofloading().execute(id);
		}
	}
	
	
		//push//

		/**
		 * Called when the activity receives a new intent.
		 */
		public void onNewIntent(Intent intent)
		{

			super.onNewIntent(intent);
			Toast.makeText(getApplicationContext(), "onNewIntent", 0).show();
			Log.i("onnewintent", "yeah");
			//have to check if we've got new intent as a part of push notification
			checkMessage(intent,false);
		}



		@Override
		public void onResume()
		{
		super.onResume();
		Data.APP_RUNNING=true;
		//Re-register receivers on resume
		registerReceivers();
		}

		@Override
		public void onPause()
		{
		super.onPause();
		Data.APP_RUNNING=false;
		//Unregister receivers on pause
		unregisterReceivers();
		}

		//Push message receiver
		private BroadcastReceiver mReceiver = new BasePushMessageReceiver()
		{
			@Override
			protected void onMessageReceive(Intent intent)
			{
				//JSON_DATA_KEY contains JSON payload of push notification.
				Toast.makeText(getApplicationContext(), "broadcastReciever:"+intent.getExtras().getString(JSON_DATA_KEY), 0).show();
				doOnMessageReceive(intent.getExtras().getString(JSON_DATA_KEY),false);
			}
		};

		NotificationCompat.Builder notification;
		PendingIntent pIntent;
		NotificationManager manager;
		Intent resultIntent;
		TaskStackBuilder stackBuilder;
		public void doOnMessageReceive(String message,boolean showNotification)
		{
			Toast.makeText(getApplicationContext(), "New Notification", 0).show();

			if(showNotification)
			{
				Intent intent= new Intent(this,Notification.class);
				intent.putExtra("json", message);
				
				System.out.println(message);
				
				Log.i("json", message);
				startActivity(intent);
			}
		}


		//Registration receiver
		BroadcastReceiver mBroadcastReceiver = new RegisterBroadcastReceiver()
		{
			@Override
			public void onRegisterActionReceive(Context context, Intent intent)
			{
				checkMessage(intent,false);
			}
		};

		private void checkMessage(Intent intent,boolean showNotification)
		{
			if (null != intent)
			{
				if (intent.hasExtra(PushManager.PUSH_RECEIVE_EVENT))
				{
					doOnMessageReceive(intent.getExtras().getString(PushManager.PUSH_RECEIVE_EVENT),showNotification);
				}
				/*else if (intent.hasExtra(PushManager.REGISTER_EVENT))
					{
						doOnRegistered(intent.getExtras().getString(PushManager.REGISTER_EVENT));
					}
					else if (intent.hasExtra(PushManager.UNREGISTER_EVENT))
					{
						doOnUnregisteredError(intent.getExtras().getString(PushManager.UNREGISTER_EVENT));
					}
					else if (intent.hasExtra(PushManager.REGISTER_ERROR_EVENT))
					{
						doOnRegisteredError(intent.getExtras().getString(PushManager.REGISTER_ERROR_EVENT));
					}
					else if (intent.hasExtra(PushManager.UNREGISTER_ERROR_EVENT))
					{
						doOnUnregistered(intent.getExtras().getString(PushManager.UNREGISTER_ERROR_EVENT));
					}*/

				resetIntentValues();
			}
		}

		private void resetIntentValues()
		{
			Intent mainAppIntent = getIntent();

			if (mainAppIntent.hasExtra(PushManager.PUSH_RECEIVE_EVENT))
			{
				mainAppIntent.removeExtra(PushManager.PUSH_RECEIVE_EVENT);
			}
			else if (mainAppIntent.hasExtra(PushManager.REGISTER_EVENT))
			{
				mainAppIntent.removeExtra(PushManager.REGISTER_EVENT);
			}
			else if (mainAppIntent.hasExtra(PushManager.UNREGISTER_EVENT))
			{
				mainAppIntent.removeExtra(PushManager.UNREGISTER_EVENT);
			}
			else if (mainAppIntent.hasExtra(PushManager.REGISTER_ERROR_EVENT))
			{
				mainAppIntent.removeExtra(PushManager.REGISTER_ERROR_EVENT);
			}
			else if (mainAppIntent.hasExtra(PushManager.UNREGISTER_ERROR_EVENT))
			{
				mainAppIntent.removeExtra(PushManager.UNREGISTER_ERROR_EVENT);
			}

			setIntent(mainAppIntent);
		}
		public void registerReceivers()
		{
			IntentFilter intentFilter = new IntentFilter(getPackageName() + ".action.PUSH_MESSAGE_RECEIVE");

			if(broadcastPush)
				registerReceiver(mReceiver, intentFilter);

			registerReceiver(mBroadcastReceiver, new IntentFilter(getPackageName() + "." + PushManager.REGISTER_BROAD_CAST_ACTION));		
		}

		public void unregisterReceivers()
		{
			//Unregister receivers on pause
			try
			{
				unregisterReceiver(mReceiver);
			}
			catch (Exception e)
			{
				// pass.
			}

			try
			{
				unregisterReceiver(mBroadcastReceiver);
			}
			catch (Exception e)
			{
				//pass through
			}
		}








		//push//
		
		 Future<File> loadfile;
			
			
		  private class Filestoring extends AsyncTask<Void, Void, Void> {
			   	 
		        @Override
		        protected void onPreExecute() {
		            super.onPreExecute();
		           
		            // Showing progress dialog
//		            pDialog = new ProgressDialog(ListActivity1.this);
//		            pDialog.setMessage("Please wait...");
//		            pDialog.setCancelable(false);
//		            pDialog.show();
		        }
		 
		        @Override
		        protected Void doInBackground(Void... arg0) {
		          if(checkInternet()){
		        	  String url1="http://cochinfood.com/oxygen/apis/get_products.php";
		        	  loadfile=Ion.with(getApplicationContext())
		        		        .load(url1)
		        		        
		        		        .write(new File(new File(getCacheDir(),"")+"cacheFileAll.srl"));
		          }
		           
		            return null;
		        }
		 
		        @Override
		        protected void onPostExecute(Void result) {
		            super.onPostExecute(result);   
		    
		              
		        }
		 }
		  
		  public boolean checkInternet(){
				NetworkInfo ni = conmgr.getActiveNetworkInfo();
				if (ni == null) {
				    // There are no active networks.
				    return false;
				}else{
					boolean isConnected = ni.isConnected();
					return isConnected;
				}
		   }
		    


		  public class checkstatusofloading extends AsyncTask<String, String, String> {
			   	 
		        @Override
		        protected void onPreExecute() {
		            super.onPreExecute();
		           
		            // Showing progress dialog
//		            pDialog = new ProgressDialog(ListActivity1.this);
//		            pDialog.setMessage("Please wait...");
//		            pDialog.setCancelable(false);
//		            pDialog.show();
		        }
		 
		        @Override
		        protected String doInBackground(String... id) {
//		        if(appState.asynctask.getStatus()==AsyncTask.Status.RUNNING)
//		        {
//		        	this.execute();
//		        }
		        String id1=id[0];
		            return id1;
		        }
		 
		        @Override
		        protected void onPostExecute(String result) {
		            super.onPostExecute(result);   
		            startnextactivity(result);
		            
		        }
		
		  }
	

}
