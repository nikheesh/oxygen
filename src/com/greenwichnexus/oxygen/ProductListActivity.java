package com.greenwichnexus.oxygen;

import java.io.File;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.Future;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

public class ProductListActivity extends Activity {
    // adapter that holds tweets, obviously :)
    ArrayAdapter<JsonObject> tweetAdapter;
    String url;
    WebView loadgif;
  //  ProgressBar progress;
   
    int count;
    private ConnectivityManager conmgr;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        count=-1;
     

        tweetAdapter = new ArrayAdapter<JsonObject>(this, 0) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if (convertView == null)
                    convertView = getLayoutInflater().inflate(R.layout.productlistitem, null);

                // we're near the end of the list adapter, so load more items
//                if (position >= getCount() - 1)
//                	if(checkInternet())
//                    loadwithinternet();

              
                JsonObject tweet = getItem(position);

                JsonObject user = tweet.getAsJsonObject();
                String imageUrl = user.get("imagelink").getAsString();

                ImageView imageView = (ImageView)convertView.findViewById(R.id.image);

                Ion.with(imageView)
                
                // use a placeholder google_image if it needs to load from the network
                .placeholder(R.drawable.aaaaaa)
                
                .error(R.drawable.aaaaaa)
                
                // load the url
                .load(imageUrl);
                
                

                // and finally, set the name and text
                TextView prod_title = (TextView)convertView.findViewById(R.id.producttitle);
                prod_title.setText(user.get("productTitle").getAsString());

                
                TextView field11 = (TextView)convertView.findViewById(R.id.field11);
                field11.setText(user.get("field11").getAsString());

                TextView field1 = (TextView)convertView.findViewById(R.id.field1);
                field1.setText(tweet.get("field1").getAsString());
                
                TextView field22 = (TextView)convertView.findViewById(R.id.field22);
                field22.setText(user.get("field22").getAsString());

                TextView field2 = (TextView)convertView.findViewById(R.id.field2);
                field2.setText(tweet.get("field2").getAsString());
                
                TextView field33 = (TextView)convertView.findViewById(R.id.field33);
                field33.setText(user.get("field33").getAsString());

                TextView field3 = (TextView)convertView.findViewById(R.id.field3);
                field3.setText(tweet.get("field3").getAsString());
                
                TextView field44 = (TextView)convertView.findViewById(R.id.field44);
                field44.setText(user.get("field44").getAsString());

                TextView field4 = (TextView)convertView.findViewById(R.id.field4);
                field4.setText(tweet.get("field4").getAsString());
                
                TextView field55 = (TextView)convertView.findViewById(R.id.field55);
                field55.setText(user.get("field55").getAsString());

                TextView field5 = (TextView)convertView.findViewById(R.id.field5);
                field5.setText(tweet.get("field5").getAsString());
                
                TextView field66 = (TextView)convertView.findViewById(R.id.field66);
                field66.setText(user.get("field66").getAsString());

                TextView field6 = (TextView)convertView.findViewById(R.id.field6);
                field6.setText(tweet.get("field6").getAsString());
                
                
                
                return convertView;
            }
        };

        // basic setup of the ListView and adapter
        setContentView(R.layout.productlistactivity);
        conmgr = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
        loadgif=(WebView)findViewById(R.id.loadinggif);
        ListView listView = (ListView)findViewById(R.id.list);
     try{
        String catid= getIntent().getExtras().getString("catid").toString();
        String compid= getIntent().getExtras().getString("compid").toString();
         String isoffer=getIntent().getExtras().getString("isoffer").toString();
        
        	loadfromfile(catid,compid,isoffer);
        
        listView.setAdapter(tweetAdapter);
     }
     catch(Exception e)
     {
     
     }

        // authenticate and do the first load
      //  getCredentials();
    }

     Future<JsonArray> loading,loadingfile;
    Future<File> loadfile;



  
    private void loadfromfile(final String catid,final String compid,final String isoffer)
    {
    	loadingfile= Ion.with(this)
    	        .load(new File(new File(getCacheDir(),"")+"cacheFileAll.srl"))
    	        
    	        
    	        .asJsonArray()
    	        
    	        .setCallback(new FutureCallback<JsonArray>() {
    	            @Override
    	            public void onCompleted(Exception e, JsonArray result) {
    	                // this is called back onto the ui thread, no Activity.runOnUiThread or Handler.post necessary.
    	                if (e != null) {
    	                    Toast.makeText(getApplicationContext(), "Your internet is too slow...!", Toast.LENGTH_LONG).show();
    	                    return;
    	                   
    	                }
    	               //  add the tweets
    	                
    	                if(isoffer.equals("1")){
    	                	
    	                	 for (int i = 0; i < result.size(); i++) {
    	   	                	String catidf=result.get(i).getAsJsonObject().get("categoryId").getAsString();
    	   	                	String compidf=result.get(i).getAsJsonObject().get("manufacturerId").getAsString();
    	   	                	if(compid.equals(compidf) && catid.equals(catidf) && result.get(i).getAsJsonObject().get("isOffer").getAsString().equals("1")){
    	   	                		
    	   	                	  tweetAdapter.add(result.get(i).getAsJsonObject());
    	   	                		
    	   	                	}
    	   	                	
    	   	                	
    	     	                  
    	     	                }
    	                	
    	                }
    	                else
    	                {
    	                
  	                for (int i = 0; i < result.size(); i++) {
  	                	String catidf=result.get(i).getAsJsonObject().get("categoryId").getAsString();
  	                	String compidf=result.get(i).getAsJsonObject().get("manufacturerId").getAsString();
  	                	if(catid.equals(catidf) && compid.equals(compidf)){
  	                		
  	                	  tweetAdapter.add(result.get(i).getAsJsonObject());
  	                		
  	                	}
  	                	
  	                	
    	                  
    	                }
    	                }
    	              	 
    				      
    					
    	            }
    	        });
    }
    public boolean checkInternet(){
		NetworkInfo ni = conmgr.getActiveNetworkInfo();
		if (ni == null) {
		    // There are no active networks.
		    return false;
		}else{
			boolean isConnected = ni.isConnected();
			return isConnected;
		}
   }
    
//    private class Filestoring extends AsyncTask<Void, Void, Void> {
//	   	 
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//           
//            // Showing progress dialog
////            pDialog = new ProgressDialog(ListActivity1.this);
////            pDialog.setMessage("Please wait...");
////            pDialog.setCancelable(false);
////            pDialog.show();
//        }
// 
//        @Override
//        protected Void doInBackground(Void... arg0) {
//          if(checkInternet()){
//        	  String url1="http://cochinfood.com/testvyooha/temp/getimagelist.php";
//        	  loadfile=Ion.with(getApplicationContext())
//        		        .load(url1)
//        		        .write(new File(new File(getCacheDir(),"")+"cacheFile1.srl"));
//          }
//           
//            return null;
//        }
// 
//        @Override
//        protected void onPostExecute(Void result) {
//            super.onPostExecute(result);   
//    
//              
//        }
// }

}
