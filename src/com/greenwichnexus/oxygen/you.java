package com.greenwichnexus.oxygen;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;

import android.graphics.Color;
import android.graphics.PorterDuff.Mode;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.RenderPriority;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebSettings.ZoomDensity;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;




public class you extends Activity {

	String urll;
	private boolean isBackPressed=false;
	private WebView mWebView;  
	private LinearLayout mContentView;
	private FrameLayout mCustomViewContainer;
	private View mCustomView;
	ProgressBar PB;
	String blu="#0099CC";
	private WebChromeClient.CustomViewCallback mCustomViewCallback;
	FrameLayout.LayoutParams COVER_SCREEN_GRAVITY_CENTER = new FrameLayout.LayoutParams(
	        ViewGroup.LayoutParams.WRAP_CONTENT,
	        ViewGroup.LayoutParams.WRAP_CONTENT, Gravity.CENTER);

	private WebChromeClient mWebChromeClient;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    requestWindowFeature(Window.FEATURE_NO_TITLE);
	    Intent i=getIntent();
		 urll=i.getExtras().getString("id");
	    setContentView(R.layout.you);

	   

	    
	    mWebView = (WebView) findViewById(R.id.webView);
	   // mWebView.setBackgroundColor(Color.TRANSPARENT)	;    
	   // mWebView.setBackgroundColor(0);
	    mCustomViewContainer = (FrameLayout) findViewById(R.id.fullscreen_custom_content);

	    mWebChromeClient = new WebChromeClient() {


	         @Override
	        public void onShowCustomView(View view, WebChromeClient.CustomViewCallback callback)
	        {
	            // if a view already exists then immediately terminate the new one
	            if (mCustomView != null)
	            {
	                callback.onCustomViewHidden();
	                return;
	            }

	            // Add the custom view to its container.
	            mCustomViewContainer.addView(view, COVER_SCREEN_GRAVITY_CENTER);
	            mCustomView = view;
	            mCustomViewCallback = callback;

	            // hide main browser view
	            mContentView.setVisibility(View.GONE);

	            // Finally show the custom view container.
	            mCustomViewContainer.setVisibility(View.VISIBLE);
	            mCustomViewContainer.bringToFront();
	        }

	         @Override
	         public void onHideCustomView()
	         {
	             if (mCustomView == null)
	                 return;

	             // Hide the custom view.
	             mCustomView.setVisibility(View.GONE);
	             // Remove the custom view from its container.
	             mCustomViewContainer.removeView(mCustomView);
	             mCustomView = null;
	             mCustomViewContainer.setVisibility(View.GONE);
	             mCustomViewCallback.onCustomViewHidden();

	             // Show the content view.
	             mContentView.setVisibility(View.VISIBLE);
	         } 
	    };
	    ConnectivityManager cn=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nf=cn.getActiveNetworkInfo();
        if(nf != null && nf.isConnected()==true )
        {
            
           

	    WebSettings webSettings = mWebView.getSettings();
	    webSettings.setPluginState(WebSettings.PluginState.ON);
	   
	    mWebView.setWebViewClient(new WebViewClient());
	    webSettings.setJavaScriptEnabled(true);
	    mWebView.getSettings().setDomStorageEnabled(true);
	    mWebView.loadUrl(urll);
	 //   mWebView.setWebViewClient(new MyWebViewClient());
	    this.mWebView.getSettings().setUseWideViewPort(true); 
	    //this.mWebView.getSettings().setLoadWithOverviewMode(true);
	    this.mWebView.getSettings().setSupportZoom(true);
	    this.mWebView.getSettings().setDefaultZoom(ZoomDensity.FAR);
	    this.mWebView.getSettings().setBuiltInZoomControls(true);
	    this.mWebView.setInitialScale(30);
	    
	    webSettings.setLoadsImagesAutomatically(true);
	    webSettings.setDomStorageEnabled(true);
	   
	    
	    
       PB=(ProgressBar)findViewById(R.id.progressBar1);
       
       //mWebView.setWebChromeClient(new WebChromeClient()
      mWebView.setWebChromeClient(new WebChromeClient() 
        
        {
    	        @Override
              	public void onProgressChanged(WebView v, int newProgress)
              	{
              		super.onProgressChanged(v, newProgress);
              		PB.setProgress(newProgress);
              		if(newProgress==100)
              		{
              			PB.setVisibility(View.GONE);
              		
              		}
              		else
              		{
              			PB.setVisibility(View.VISIBLE);
              		}
              	}
			
        	
        	
        });
         
      //  
        
        
        }
        
        
        
        else
        {  
        	
            
            Toast.makeText(this, "You are offline", Toast.LENGTH_LONG).show();
            
            Toast.makeText(this, "Please check the connection settings", Toast.LENGTH_LONG).show();
            finish();
        }
	 
        
	}
	
	
	@Override
    public void onBackPressed() {
        if (mWebView.canGoBack())
        	mWebView.goBack();
        else
            super.onBackPressed();
    }
	
	
	
	
	
	
	
	
}