package com.greenwichnexus.oxygen;

import java.io.File;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;

import com.koushikdutta.async.future.Future;
import com.koushikdutta.ion.Ion;

public class FileStoring extends AsyncTask<Void, Void, Void> {
  	 private  Future<File> loadfile;
  	 Context context;
  	private ConnectivityManager conmgr;
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        conmgr = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        // Showing progress dialog
//        pDialog = new ProgressDialog(ListActivity1.this);
//        pDialog.setMessage("Please wait...");
//        pDialog.setCancelable(false);
//        pDialog.show();
    }

    @Override
    protected Void doInBackground(Void... arg0) {
      if(checkInternet()){
    	  String url1="http://cochinfood.com/oxygen/apis/get_products.php";
    	  loadfile=Ion.with(context)
    		        .load(url1)
    		        
    		        .write(new File(new File(context.getCacheDir(),"")+"cacheFileAll.srl"));
      }
       
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);   

          
    }
    
    public boolean checkInternet(){
		NetworkInfo ni = conmgr.getActiveNetworkInfo();
		if (ni == null) {
		    // There are no active networks.
		    return false;
		}else{
			boolean isConnected = ni.isConnected();
		
			return isConnected;
		}
   }
}
