package com.greenwichnexus.oxygen;



import java.io.File;


import com.koushikdutta.async.future.Future;
import com.koushikdutta.ion.Ion;

import android.app.ActionBar;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

public class ApplicationClass extends Application {
	
    private ConnectivityManager conmgr;
	
	 Future<File> loadfile;
	 
	 public ProgressDialog proDialog;
		public void startLoading() {
		    proDialog = new ProgressDialog(this);
		    proDialog.setMessage(" Please Wait.....");
		    proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		    proDialog.setCancelable(false);
		    proDialog.show();
		}

		public void stopLoading() {
		    proDialog.dismiss();
		    proDialog = null;
		}
	 
	 
	 public boolean checkInternet(){
			NetworkInfo ni = conmgr.getActiveNetworkInfo();
			if (ni == null) {
			    // There are no active networks.
			    return false;
			}else{
				boolean isConnected = ni.isConnected();
				return isConnected;
			}
	   }
	  public class Filestoring extends AsyncTask<Void, Void, Void> {
		   	 
	        @Override
	        protected void onPreExecute() {
	            super.onPreExecute();
	           
	            // Showing progress dialog
//	            pDialog = new ProgressDialog(ListActivity1.this);
//	            pDialog.setMessage("Please wait...");
//	            pDialog.setCancelable(false);
//	            pDialog.show();
	        }
	 
	        @Override
	        protected Void doInBackground(Void... arg0) {
	          if(checkInternet()){
	        	  String url1="http://cochinfood.com/oxygen/apis/get_products.php";
	        	  loadfile=Ion.with(getApplicationContext())
	        		        .load(url1)
	        		        .write(new File(new File(getCacheDir(),"")+"cacheFileAll.srl"));
	          }
	           
	            return null;
	        }
	 
	        @Override
	        protected void onPostExecute(Void result) {
	            
	        	Log.d("ASYNC", "Async Task finished");
	        	super.onPostExecute(result);   
	            if(proDialog!=null){
	    stopLoading();
	            }   
	        }
	 }

	  public Filestoring asynctask;
	  @Override
		public void onCreate() {
			super.onCreate();
			 conmgr = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
				// setting time for launcher
				asynctask = new Filestoring();

		}
	  
	  
}
