package com.greenwichnexus.oxygen;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;


import com.koushikdutta.async.future.Future;
import com.koushikdutta.ion.Ion;




import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

public class MainActivity extends Activity {
	
	
	
	private boolean isBackPressed = false;
	private static int SPLASH_TIME_OUT=1500;
    private ConnectivityManager conmgr;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		 requestWindowFeature(Window.FEATURE_NO_TITLE);
	 	 getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		 conmgr = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
		// setting time for launcher
		// new Filestoring().execute();
		 
		 ApplicationClass appState = ((ApplicationClass)getApplicationContext());
	     if(appState.asynctask.getStatus()==AsyncTask.Status.PENDING)    
		  appState.asynctask.execute();
		 
				new Handler().postDelayed(new Runnable(){
					public void run()
					{   if(!isBackPressed)
					{
						Intent i=new Intent(MainActivity.this,First.class);
						startActivity(i);
						finish();
					}}
				},SPLASH_TIME_OUT);
			}
			
       
    
	
	 @Override
	    public boolean onKeyDown(int keyCode, KeyEvent event) {
	        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
	            isBackPressed = true;
	            finish();
	        }
	        return super.onKeyDown(keyCode, event);

	    }
    @Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		Data.APP_RUNNING=false;
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Data.APP_RUNNING=true;
	}
	
	
	
	 public boolean checkInternet(){
			NetworkInfo ni = conmgr.getActiveNetworkInfo();
			if (ni == null) {
			    // There are no active networks.
			    return false;
			}else{
				boolean isConnected = ni.isConnected();
				return isConnected;
			}
	   }
	    
//	 Future<File> loadfile;
//	
//	
//	  private class Filestoring extends AsyncTask<Void, Void, Void> {
//		   	 
//	        @Override
//	        protected void onPreExecute() {
//	            super.onPreExecute();
//	           
//	            // Showing progress dialog
////	            pDialog = new ProgressDialog(ListActivity1.this);
////	            pDialog.setMessage("Please wait...");
////	            pDialog.setCancelable(false);
////	            pDialog.show();
//	        }
//	 
//	        @Override
//	        protected Void doInBackground(Void... arg0) {
//	          if(checkInternet()){
//	        	  String url1="http://cochinfood.com/oxygen/apis/get_products.php";
//	        	  loadfile=Ion.with(getApplicationContext())
//	        		        .load(url1)
//	        		        .write(new File(new File(getCacheDir(),"")+"cacheFileAll.srl"));
//	          }
//	           
//	            return null;
//	        }
//	 
//	        @Override
//	        protected void onPostExecute(Void result) {
//	            super.onPostExecute(result);   
//	    
//	              
//	        }
//	 }
}
