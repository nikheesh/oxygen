package com.greenwichnexus.oxygen;

import java.io.File;
import java.util.ArrayList;

import com.google.gson.JsonArray;
import com.koushikdutta.async.future.Future;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class OfferActivity extends Activity {
    // adapter that holds tweets, obviously :)
    ArrayAdapter<ListCompany> tweetAdapter;
    ArrayList<ListCompany> listcompany;
    ListCompany listcomp;
    String url;
    WebView loadgif;
  //  ProgressBar progress;
   
    int count;

    @Override
    public void onCreate(Bundle savedInstanceState) {
    	 requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        
        count=-1;
     
        int sdk = android.os.Build.VERSION.SDK_INT;
        
        tweetAdapter = new ArrayAdapter<ListCompany>(this, 0) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if (convertView == null)
                    convertView = getLayoutInflater().inflate(R.layout.category_item, null);

             
                Button handle = (Button)convertView.findViewById(R.id.categoryname);
              handle.setText(this.getItem(position).name);
              final ListCompany listcompany=this.getItem(position);
              handle.setOnClickListener(new OnClickListener() {
      			
      			@Override
      			public void onClick(View v) {
      				Intent i=new Intent(getApplicationContext(),CompanyNamesListActivity.class);
      				i.putExtra("catid",listcompany.id);
      				//i.putExtra("compid", listcompany.id);
      				i.putExtra("id","offers");
      				startActivity(i);
      				// TODO Auto-generated method stub
      				
      			}
      		});

                return convertView;
            }
        };

        // basic setup of the ListView and adapter
        setContentView(R.layout.categorylist);
       
    //    loadgif=(WebView)findViewById(R.id.loadinggif);
        ListView listView = (ListView)findViewById(R.id.list_category);
        String catname= getIntent().getExtras().getString("id").toString();
        
        	loadfromfile();
        
        listView.setAdapter(tweetAdapter);

    }

      Future<JsonArray> loading,loadingfile;
    


    private void loadfromfile()
    {
    	loadingfile= Ion.with(this)
    	        .load(new File(new File(getCacheDir(),"")+"cacheFileAll.srl"))
    	        
    	        
    	        .asJsonArray()
    	        
    	        .setCallback(new FutureCallback<JsonArray>() {
    	            @Override
    	            public void onCompleted(Exception e, JsonArray result) {
    	                // this is called back onto the ui thread, no Activity.runOnUiThread or Handler.post necessary.
    	                if (e != null) {
    	                    Toast.makeText(getApplicationContext(), "Your connection is too slow, please wait few seconds while we are fetching latest data", Toast.LENGTH_LONG).show();
    	                    return;
    	                   
    	                }
    	              
    	               listcompany=new ArrayList<ListCompany>();
    	              
    	            	   
    	            	   for (int i = 0; i < result.size(); i++) {
    	  	                	if(result.get(i).getAsJsonObject().get("isOffer").getAsString().equals("1"))
    	  	                	{
    	  	                		//Log.e(" =======catid========",result.get(i).getAsJsonObject().get("categoryId").getAsString());
    	  	                		String id=result.get(i).getAsJsonObject().get("categoryId").getAsString();
    	  	                		Log.e("==items id==", id);
    	  	                		String name=result.get(i).getAsJsonObject().get("categoryName").getAsString();
    	  	                		Log.e("==items name==", name);
    	  	                		if(listcompany.size()==0)
    	  	                			{
    	  	                	listcomp =new ListCompany();
    	  	              
    	  	                	listcomp.id=id;
    	  	                	listcomp.catid="";
    	  	                //	Log.e("====man  id", listcomp.id);
    	  	                	listcomp.name=name;
    	  	                	listcompany.add(listcomp);
    	  	                	//Log.e("====man  id", listcomp.name);
    	  	              }
    	  	              else {
    	  	            	  
    	  	            	  int f=0;
    	  	            	  for(int j=0;j<listcompany.size();j++)
    	  	            	  		{ 		if(listcompany.get(j).id.equals(id))
    	  	            	  					{f=1;
    	  	            	  					break;
    	  	            	  					}
    	  	            	  		}
    	  	            	  
    	  	            	  if(f==0)
    	  	            	  	{
    	  	            		  listcomp =new ListCompany();
    	  	            	  listcomp.id=id;
    	  	            	listcomp.catid="";
    		                	//Log.e("====man  id", listcomp.id);
    		                	listcomp.name=name;
    	  	            		listcompany.add(listcomp);  
    	  	            	  	}
    	  	            	  
    	  	              	}
    	  	                	
    	  	                	}
    	    	                   // tweetAdapter.add(result.get(i).getAsJsonObject());
    	    	                }
    	            	   
    	            	   
    	               
    	              
//  	            
  	            for (int i = 0; i < listcompany.size(); i++) {
  	            	tweetAdapter.add(listcompany.get(i));
  	            	Log.e("ites",listcompany.get(i).toString());
  	            	
  	            	
  	            }  
    					
    	            }
    	        });
    }
   

}
