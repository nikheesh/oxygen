package com.greenwichnexus.oxygen;



import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnCancelListener;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.RenderPriority;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;



public class webo extends Activity {

	private final static String HOST = "www.almullatravel.com/";
	private WebView wb;
    String urll;
    
    ProgressBar PB;
    
    ImageView yout,webb,fbk,twt,cal,gplus;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    requestWindowFeature(Window.FEATURE_NO_TITLE);
	    
	    
	    Intent i=getIntent();
		 urll=i.getExtras().getString("id");
	    setContentView(R.layout.webo);
	    wb = (WebView) findViewById(R.id.webView1);
	    PB=(ProgressBar) findViewById(R.id.progressBar1);
		
	 	
	    
	
	
		
		
	    //Make the webview invisible
	    
	    ConnectivityManager cn=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nf=cn.getActiveNetworkInfo();
        if(nf != null && nf.isConnected()==true )
        {
           
	    wb.setVisibility(View.INVISIBLE);
	    WebSettings webSettings = wb.getSettings();
	    //webSettings.setPluginState(WebSettings.PluginState.ON);
	   // webSettings.setJavaScriptEnabled(true);
	  // wb.getSettings().setDomStorageEnabled(true);
	   // webSettings.setLoadsImagesAutomatically(true);
	    
	  
	 
	    wb.setWebViewClient(new WebViewClient());
	    webSettings.setJavaScriptEnabled(true);
	    wb.getSettings().setDomStorageEnabled(true);
	    this.wb.getSettings().setUseWideViewPort(true); 
	    wb.setWebChromeClient(new WebChromeClient()
	    {
	    	
	       public void onPageFinished(WebView view, String url){
	            //Inject javascript code to the url given
	            //Not display the element
	            wb.loadUrl("javascript:(function(){"+"document.getElementById('Id').style.display ='none';"+"})()");
	            //Call to a function defined on my myJavaScriptInterface 
	            wb.loadUrl("javascript: window.CallToAnAndroidFunction.setVisible()");
	       }
	              
	        
	    	
	    	  public void onProgressChanged(WebView wb, int newProgress)
	        
	          	{
	          		super.onProgressChanged(wb, newProgress);
	          		PB.setProgress(newProgress);
	          		if(newProgress==100)
	          		{
	          			PB.setVisibility(View.GONE);
	          			 wb.loadUrl("javascript:(function(){"+"document.getElementById('Id').style.display ='none';"+"})()");
	          			 wb.loadUrl("javascript: window.CallToAnAndroidFunction.setVisible()");
	          		}
	          		else
	          		{
	          			PB.setVisibility(View.VISIBLE);
	          		}
	          		
	          		
	          	}
		        
		        
	        
	        
	    });
	    
       
	    
	    wb.loadUrl(urll);
	    
	    
	    
        }
        else
        {
        
        	
        	 Toast.makeText(this, "You are offline", Toast.LENGTH_LONG).show();
             
             Toast.makeText(this, "Please check the connection settings", Toast.LENGTH_LONG).show();
             finish();
        }
	    

	    //Add a JavaScriptInterface, so I can make calls from the web to Java methods
	    wb.addJavascriptInterface(new myJavaScriptInterface(), "CallToAnAndroidFunction");
	    ////////////////////////
	    
	    
	   
	    
	    
	}

	 public class myJavaScriptInterface {
	     @JavascriptInterface
	     public void setVisible(){
	         runOnUiThread(new Runnable() {

	            @Override
	            public void run() {
	                wb.setVisibility(View.VISIBLE);                 
	            }
	        });
	     }

	 }}
