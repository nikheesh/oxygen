package com.greenwichnexus.oxygen;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;

public class Socialcntct extends Activity {
	
	ImageView fb,twt,youtub,webb;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		 requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.social);
		
		fb=(ImageView) findViewById(R.id.fb);
		fb.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try
				{
					Intent i=new Intent(Intent.ACTION_VIEW,Uri.parse("fb://profile/215647178487499"));
					startActivity(i);

				}catch(Exception e)
				{    
					
					
				    /*Intent i=new Intent(Intent.ACTION_VIEW,Uri.parse("https://www.facebook.com/KeralaTravelMart"));
					startActivity(i);*/
					Intent i = new Intent(getApplicationContext(), you.class);
					i.putExtra("id","https://www.facebook.com/oxygendigitalshop");
					startActivity(i);
				
				}
				
			}
		});
		
		twt=(ImageView) findViewById(R.id.twitter);
		twt.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try
				{
					Intent i=new Intent(Intent.ACTION_VIEW,Uri.parse("twitter://user?screen_name=Oxygen_Digital"));
					startActivity(i);

				}catch(Exception e)
				{    
					
					/*Intent i=new Intent(Intent.ACTION_VIEW,Uri.parse("https://twitter.com/KTMsociety"));
					startActivity(i);*/
					Intent i = new Intent(getApplicationContext(), you.class);
					i.putExtra("id","https://twitter.com/Oxygen_Digital");
					startActivity(i);
					
				}
				
			}
		});
		
		youtub=(ImageView) findViewById(R.id.youtube);
		youtub.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try
				{
					
					Intent intent=null;     
					try {
					        intent =new Intent(Intent.ACTION_VIEW);
					        intent.setPackage("com.google.android.youtube");
					        intent.setData(Uri.parse("https://www.youtube.com/channel/UCeVcmPP8kQHEPwGQ-fUhgOg/feed"));
					        startActivity(intent);
					    } catch (ActivityNotFoundException e) {
					    	Intent i = new Intent(getApplicationContext(), you.class);
							i.putExtra("id","https://www.youtube.com/channel/UCeVcmPP8kQHEPwGQ-fUhgOg/feed");
							startActivity(i);
					    	
					       
					    }
					
				}catch(Exception e)
				{    
					
					
					
					
					Intent i = new Intent(getApplicationContext(), you.class);
					i.putExtra("id","https://www.youtube.com/channel/UCeVcmPP8kQHEPwGQ-fUhgOg/feed");
					startActivity(i);
					
				}	
				
			}
		});
		webb=(ImageView) findViewById(R.id.web);
		webb.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent i = new Intent(getApplicationContext(), you.class);
				i.putExtra("id","http://www.oxygendigitalshop.in/");
				startActivity(i);
				
			}
		});
	}

}
